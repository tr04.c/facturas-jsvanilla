/*recordar importar date.js de users para la fecha*/

const LIST = "list";
const EDIT = "edit";

let estado = {
    "action" : LIST,
    "elem" : null
};


// CARGA DE DATOS
const facturas = [
{ 
  "id" : ' <i class="fas fa-file-invoice-dollar"></i>&nbsp;&nbsp;Nº 2104245XX1',
  "cliente"  :  "Celia Casado", 
  "importe"   :  "20 €",
  "Fecha" : "05-05-2019"
},
{ 
  "id" : '<i class="fas fa-file-invoice-dollar"></i>&nbsp;&nbsp;Nº 2104245XX2',
  "cliente"  :  "Guillermo Montero", 
  "importe"   :  "20 €",
  "Fecha" : "10-05-2019"
},
{ 
  "id" : '<i class="fas fa-file-invoice-dollar"></i>&nbsp;&nbsp;Nº 2104245XX3',
  "cliente"  :  "Alex Algar", 
  "importe"   :  "20 €",
  "Fecha" : "12-05-2019"
},
{ 
  "id" : '<i class="fas fa-file-invoice-dollar"></i>&nbsp;&nbsp;Nº 2104245XX4',
  "cliente"  :  "Gustavo Segovia", 
  "importe"   :  "20 €",
  "Fecha" : "22-05-2019"
},
{ 
  "id" : '<i class="fas fa-file-invoice-dollar"></i>&nbsp;&nbsp;Nº 2104245XX5',
  "cliente"  :  "Carlos San Juan", 
  "importe"   :  "20 €",
  "Fecha" : "12-05-2019"
},
{ 
  "id" : '<i class="fas fa-file-invoice-dollar"></i>&nbsp;&nbsp;Nº 2104245XX6',
  "cliente"  :  "Cristina Algar", 
  "importe"   :  "20 €",
  "Fecha" : "02-06-2019"
},
{ 
  "id" : '<i class="fas fa-file-invoice-dollar"></i>&nbsp;&nbsp;Nº 2104245XX7',
  "cliente"  :  "Celia Gomez", 
  "importe"   :  "20 €",
  "Fecha" : "09-05-2019"
},
{ 
  "id" : '<i class="fas fa-file-invoice-dollar"></i>&nbsp;&nbsp;Nº 2104245XX8',
  "cliente"  :  "Ana Gomez", 
  "importe"   :  "20 €",
  "Fecha" : "02-01-2019"
},
{ 
  "id" : '<i class="fas fa-file-invoice-dollar"></i>&nbsp;&nbsp;Nº 2104245XX9',
  "cliente"  :  "Jose Hernandez", 
  "importe"   :  "20 €",
  "Fecha" : "08-05-2019"
},
{ 
  "id" : '<i class="fas fa-file-invoice-dollar"></i>&nbsp;&nbsp;Nº 2104245XX10',
  "cliente"  :  "Carlos Cuenca", 
  "importe"   :  "20 €",
  "Fecha" : "08-05-2019"
},
{ 
  "id" : '<i class="fas fa-file-invoice-dollar"></i>&nbsp;&nbsp;Nº 2104245XX11',
  "cliente"  :  "Maria del Olmo", 
  "importe"   :  "20 €",
  "Fecha" : "03-02-2019"
},
{ 
  "id" : '<i class="fas fa-file-invoice-dollar"></i>&nbsp;&nbsp;Nº 2104245XX',
  "cliente"  :  "Maria de los Pino", 
  "importe"   :  "20 €",
  "Fecha" : "25-02-2019"
}
];

function update(){
    // Revertimos los cambios en los botones y estado
    document.querySelector("#guardar").innerHTML = "Guardar";
    estado.action = LIST;
    // Modificamos el DOM
    let fila = estado.elem.childNodes;
    fila[1].innerHTML = document.querySelector("#inputCliente").value;
    fila[2].innerHTML = document.querySelector("#inputImporte").value;
    fila[3].innerHTML = document.querySelector("#inputFecha").value;
    // Reactivamos el boton de borrar
    let delBtn = estado.elem.querySelector("#delBtn");
    enableDelBtn(delBtn);
    clearForm();
}

function save(){

    let cliente = document.querySelector("#inputCliente").value;
    let importe = document.querySelector("#inputImporte").value;
    let fecha = document.querySelector("#inputFecha").value;
    // Calculamos el nuevo id a mano
    let tabla = document.querySelector("#tabla_facturas");
    let id = ' <i class="fas fa-file-invoice-dollar"></i>&nbsp;&nbsp;Nº 2104245XX'+tabla.rows.length;

    let fact = {};
    fact.id = id;
    fact.cliente = cliente;
    fact.importe = importe;
    fact.fecha = fecha;
    
    addFactura(fact);

    facturas.push(fact);
    console.log(facturas);
    localStorage.setItem("facturas", JSON.stringify(facturas));

    clearForm();
}

function clearForm(){
    document.querySelector("#inputCliente").value = "";
    document.querySelector("#inputImporte").value = "";
    document.querySelector("#inputFecha").value = "";
}

function addFactura(_fact) {
    let tabla = document.querySelector("#tabla_facturas");
    let row = tabla.insertRow(tabla.rows.length);
    for (let dato in _fact){
        let celda = row.insertCell();
        celda.innerHTML = _fact[dato];
    }

    let celdaLinks = row.insertCell();
    let linkCeldaEdit = document.createElement("btn");
    linkCeldaEdit.id = "editBtn";
    linkCeldaEdit.classList.add('btn', 'btn-primary', 'm-r-1em', 'editar');
    linkCeldaEdit.innerHTML = "Editar";
    linkCeldaEdit.addEventListener("click", edit);

    celdaLinks.appendChild(linkCeldaEdit);
    
    let linkCeldaBorrar = document.createElement("btn");
    linkCeldaBorrar.id = "delBtn";
    linkCeldaBorrar.classList.add('btn', 'btn-danger', 'm-r-1em', 'borrar');
    linkCeldaBorrar.addEventListener("click", borrar);
    linkCeldaBorrar.innerHTML = "Borrar";
    celdaLinks.appendChild(linkCeldaBorrar);
}

// EDITAR
function edit(e) {
    estado.action = EDIT;

    let fila = e.target.parentNode.parentNode.childNodes;

    document.querySelector("#inputCliente").value = fila[1].innerHTML;
    document.querySelector("#inputImporte").value = fila[2].innerHTML;
    document.querySelector("#inputFecha").value = fila[3].innerHTML;

    document.querySelector("#guardar").innerHTML = "Actualizar";
    // Restauramos el boton borrar en caso necesario
    if(estado.elem != null) 
    {
        let prevDelBtn = estado.elem.querySelector("#delBtn");
        enableDelBtn(prevDelBtn);
    }
    // Añadimos la fila a nuestro objeto estado
    estado.elem = e.target.parentNode.parentNode;

    // Desactivamos el boton de Borrar
    let delBtn = e.target.parentNode.querySelector("#delBtn");
    delBtn.setAttribute("disabled", true);
    delBtn.removeEventListener("click", borrar);
    delBtn.classList.add("btn-warning");
    delBtn.classList.remove("btn-danger");
    
}

function borrar(e){
    let fila = e.target.parentNode.parentNode;
    fila.parentNode.removeChild(fila);
}

function enableDelBtn(button) {
    button.removeAttribute("disabled");
    button.addEventListener("click", borrar);
    button.classList.remove("btn-warning");
    button.classList.add("btn-danger");
}

// GUARDAR
let btnGuardar = document.querySelector("#guardar");

btnGuardar.onclick = function() {
    switch (estado.action) {
        case EDIT:
            update();
            break;
        default:
            save();
    }

}

//init();
console.log(facturas);

facturas.forEach(addFactura);
